const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    schema: {
        type: Number,
        required: true,
    },
    name: {
        type: String,
        required: true,
        lowercase: true,
    },
})

module.exports = mongoose.model('User', userSchema);