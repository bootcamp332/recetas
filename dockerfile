FROM node:18

RUN mkdir -p /apirecetas

COPY . /apirecetas

EXPOSE 3000

CMD ["node", "/apirecetas/index.js"]

